import re
import collections
import itertools

f = open('test.html', 'r')

content = f.read()

parts_pattern = "[А-я ]+[/.,!/?]"
parts = re.findall(parts_pattern, content)


words_pattern = "\w{4,}"
link_pattern = re.compile('http.?://\S+')
links = link_pattern.findall(content)
domains_pattern = 'http.?://([A-z.0-9]+)'

domains = re.findall(domains_pattern, content)
domain_counts = dict((x, domains.count(x)) for x in domains)
sorted_domains = sorted(domain_counts.items(), key=lambda x: x[1], reverse=True)

words = re.findall(words_pattern, content)
word_dict = dict((x, words.count(x)) for x in words)
sorted_dict = sorted(word_dict.items(), key=lambda x: x[1], reverse=True)

print(parts)
print(words)
print(sorted_dict[0:10])

print(links)
print(domains)
print(sorted_domains[0:1])

print(link_pattern.sub('"Ссылка отобразится после регистрации"', content))
