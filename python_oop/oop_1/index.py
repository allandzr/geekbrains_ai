from classes import Word
from classes import Sentence
from classes import Noun
from classes import Verb

w = Word('Текст')
words = []

words.append(w)
words.append(Noun("собака"))
words.append(Verb("ела"))
words.append(Noun("колбасу"))
words.append(Noun("кот"))

sentence = Sentence(words, [1, 2, 3, 0])

for w in words:
    print(w)

print(sentence.show())
print(sentence.show_parts())
