class Word:
    text = ''
    _grammar = ''

    def __init__(self, text):
        self.text = text

    def __str__(self):
        return self.text + ' - ' + self.get_part() + '(' + self.get_full_part() + ')'

    def get_part(self):
        return self._grammar

    def get_full_part(self):
        return ''


class Noun(Word):
    _grammar = "гл"

    def get_full_part(self):
        return 'Глагол'


class Verb(Word):
    _grammar = "сущ"

    def get_full_part(self):
        return 'Существительное'


class Sentence:
    words = []
    content = [1, 2, 3]

    def __init__(self, words, content):
        self.words = words
        self.content = content

    def get_word_by_index(self, i):
        return i < len(self.words) and self.words[i] or None

    def show(self):
        text = ''
        for i in self.content:
            word = self.get_word_by_index(i)
            if word:
                text += word.text + ' '

        return text

    def show_parts(self):
        parts = []
        for i in self.content:
            word = self.get_word_by_index(i)
            if word:
                parts.append(word.get_full_part())

        return list(dict.fromkeys(parts))
