class Word:
    def __init__(self, arg1, arg2):
        self.text = arg1
        self.part = arg2


class Sentence:
    def __init__(self, words):
        self.content = words

    def show(self):
        s = ''
        for n in self.content:
            s += words[n].name + " "
        return s

    def show_parts(self):
        li = []
        for n in self.content:
            if not words[n].part() in li:
                li.append(words[n].part())
        return li


class Noun(Word):
    def __init__(self, arg1):
        self.name = arg1
        self.__grammar = "сущ"

    def part(self):
        return 'существительное'


class Verb(Word):
    def __init__(self, arg1):
        self.name = arg1
        self.__grammar = "гл"

    def part(self):
        return 'глагол'


words = []
words.append(Noun("собака"))
words.append(Verb("ела"))
words.append(Noun("колбасу"))
words.append(Noun("кот"))

sentence = Sentence([0, 1, 2])
print(sentence.show())
print(sentence.show_parts())

# words = [["собака", "сущ"],
#          ["ела", "глагол"],
#          ["колбасу", "сущ"],
#          ["вечером", "наречие"]]
#
# for n in range(len(words)):
#     words[n] = Word(words[n][0], words[n][1])

