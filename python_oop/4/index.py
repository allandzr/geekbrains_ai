import requests
import json
import os


def get_cities_data():
    cities_link = "http://api.travelpayouts.com/data/ru/cities.json"
    return json.loads(requests.get(cities_link).text)


if not os.path.exists('cities.json'):
    print('Загрузка данных...')
    data = get_cities_data()
    with open('cities.json','w') as f:
        json.dump(data, f)
else:
    with open('cities.json','r') as f:
        data = json.loads(f.read())

city_name = input('Введите город\n')


for d in data:
    if d['name'] == city_name:
        print(d)
        print(d['code'])
        break











