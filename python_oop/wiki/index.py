from wiki_request import get_page, get_page_by_link
import re


def get_words(content):
    return re.findall('[А-я]{3,}', content)


def get_words_rate(words, base_words={}):
    for w in words:
        base_words[w] = base_words.get(w, 0) + 1
    return base_words


def get_links(html):
    return re.findall('<a\s+href="(/wiki/[A-z0-9%:]+)"\s.*/a>', html)


def main():
    topic = "Россия"
    page = get_page(topic)
    links = get_links(page)
    print(f"Links found {len(links)}")
    words = get_words(page)
    print(f"Words found {len(words)}")
    words_rate = get_words_rate(words)

    for link in links[0:10]:
        print(f"Parse link: {link}")
        words = get_words(get_page_by_link(link))
        print(f"Words found {len(words)}")
        words_rate = get_words_rate(words, words_rate)

    words_rate = list(words_rate.items())
    words_rate.sort(key=lambda x: -x[1])

    print(words_rate)


main()
