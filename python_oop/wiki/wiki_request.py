from requests import get


def get_page_by_link(link):
    return get(f"https://ru.wikipedia.org{link}").text

def get_link(topic):
    return f"https://ru.wikipedia.org/wiki/{topic}"


def get_page(topic):
    return get(get_link(topic)).text




