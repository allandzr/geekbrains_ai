def max_number3(n1, n2, n3):
    return max(n1, n2, n3)


def max_number(*args):
    return max(args)


print(max_number3(5, 13, 10))
print(max_number(3, 6, 10, 23))
print(max(4, 6, 7, 2, 3))
