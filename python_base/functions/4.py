import random


def armor(a, d):
    return round(d / a, 2)


def attack(player, enemy):
    player['health'] = player['health'] - armor(player['armor'], enemy['damage'])
    return player


for i in range(3):
    player = {'name': 'Player' + str(i), 'health': random.randint(1, 100), 'damage': random.randint(1, 100),
              'armor': 1.2}
    enemy = {'name': 'Enemy' + str(i), 'health': random.randint(1, 100), 'damage': random.randint(1, 100)}

    print(player, enemy)
    print(attack(player, enemy));
