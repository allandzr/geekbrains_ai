import random


def attack(player, enemy):
    player['health'] = player['health'] - enemy['damage']
    return player


for i in range(5):
    player = {'name': 'Player' + str(i), 'health': random.randint(1, 100), 'damage': random.randint(1, 100)}
    enemy = {'name': 'Enemy' + str(i), 'health': random.randint(1, 100), 'damage': random.randint(1, 100)}

    print(player, enemy)
    print(attack(player, enemy));
