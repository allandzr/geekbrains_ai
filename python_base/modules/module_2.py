import random


def random_item(list):
    if not list:
        return None
    return random.choice(list)


if __name__ == "__main__":
    test_list = [random.randint(1, 100) for i in range(10)]
    print(test_list)
    print(random_item(test_list))
    print(random_item([]))
