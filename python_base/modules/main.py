import os
import random

from module_1 import create_dirs, delete_dirs
from module_2 import random_item


create_dirs()
print(os.listdir())
delete_dirs()
print(os.listdir())

test_list = [random.randint(1, 100) for i in range(10)]
print(test_list)
print(random_item(test_list))
print(random_item([]))

