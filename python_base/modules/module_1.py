import os


def create_dirs(prefix='dir'):
    for i in range(1, 10):
        path = os.path.join(os.getcwd(), f'dir_{i}')
        if not os.path.exists(path):
            os.mkdir(path)


def delete_dirs(prfix='dir'):
    for i in range(1, 10):
        path = os.path.join(os.getcwd(), f'dir_{i}')
        if os.path.exists(path):
            os.rmdir(path)


if __name__ == "__main__":
    create_dirs()
    print(os.listdir())
    delete_dirs()
    print(os.listdir())
