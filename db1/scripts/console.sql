drop database if EXISTS online_course;
create database if not EXISTS online_course;
use online_course;

-- Пользователи
create table if not EXISTS users
(
    id         int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email      VARCHAR(100) NOT NULL,
    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()

);

-- Таблица файлов для уроков или любых других файлов
create table if not EXISTS files
(
    id         int UNSIGNED                              not null AUTO_INCREMENT PRIMARY key,
    file_name  varchar(255)                              not null,
    file_path  varchar(255)                              not null,
    file_type  enum ('image','video','audio','document') not null,
    metadata   json,

    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()
);

-- таблица чатов (каналов) для сообщений, могут быть разные чаты например пользователь-эксперт курса, пользователь-проверяющий ДЗ, все пользователи потока курса
-- чаты связаны с создаюзими их сущностями через type+target_id
create table if not exists chat_channels
(
    id         int unsigned not null auto_increment primary key,
    type       enum ('course_expert','homework','course_stream') default 'course_expert',
    status     enum ('open','closed')                            default 'open',
    target_id  int unsigned not null, -- ссылка на запись с котрой связан чат, ключ зависит от типа и ид

    created_at datetime                                          default NOW(),
    updated_at datetime                                          default NOW() on update NOW()
);

create table if not exists chat_messages
(
    id                int unsigned not null auto_increment primary key,
    chat_channel_id   int unsigned not null,
    user_id           int unsigned not null,
    message           text         not null,
    parent_message_id int unsigned default null,
    constraint chat_message_parent_fk foreign key (parent_message_id) references chat_messages (id),
    constraint chat_message_channel_fk foreign key (chat_channel_id) references chat_channels (id) on delete cascade on update cascade,
    created_at        datetime     default NOW(),
    updated_at        datetime     default NOW() on update NOW()

);

-- таблица метаданных о чатах, используется для удобноговывода статистики чата, без пересчтеа
create table if not exists chat_channels_meta
(
    chat_channel_id   int unsigned not null,
    user_id           int unsigned not null,
    message_count     int unsigned not null default 0,
    last_message_id   int unsigned,
    last_message_time datetime,

    primary key chat_channels_meta__pk (chat_channel_id, user_id),
    constraint chat_channel_meta_channel_fk foreign key (chat_channel_id) references chat_channels (id) on delete CASCADE on update cascade,
    constraint chat_channel_meta_user_fk foreign key (user_id) references users (id) on delete CASCADE on update cascade
    #  constraint chat_channel_meta_last_message_fk foreign key (last_message_id) references chat_messages (id) on delete set null on update set null

);

create trigger chat_channel_meta_update
    after insert
    on chat_messages
    for each row
begin
    insert into chat_channels_meta (chat_channel_id, user_id, last_message_id, last_message_time, message_count)
    values (NEW.chat_channel_id, NEW.user_id, NEW.id, NEW.created_at, 1)
    on duplicate key
        update message_count = message_count + 1, last_message_time=NEW.created_at, last_message_id=NEW.id;
end;

create trigger chat_channel_meta_delete
    after delete
    on chat_messages
    for each row
begin
    set @lastMessageId = null;
    set @lastMessageTime = null;
    select id, created_at
    into @lastMessageId, @lastMessageTime
    from chat_messages
    where chat_channel_id = OLD.chat_channel_id
      and user_id = OLD.user_id
    order by id desc
    limit 1;
    update chat_channels_meta
    set message_count    = message_count - 1,
        last_message_time=@lastMessageTime,
        last_message_id=@lastMessageId
    where chat_channel_id = OLD.chat_channel_id
      and user_id = OLD.user_id;
end;


-- Сами курсы
create table if not EXISTS courses
(
    id         int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name       varchar(255) not null,

    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()

);


-- Уроки курсов
create table if not exists lessons
(
    id            int UNSIGNED not null AUTO_INCREMENT PRIMARY KEY,
    name          varchar(255) not null,
    description   text,
    video_file_id int UNSIGNED,

    created_at    datetime default NOW(),
    updated_at    datetime default NOW() on update NOW(),

    CONSTRAINT lesson_video_file_fk FOREIGN KEY (video_file_id) REFERENCES files(id)
);

-- Таблица связей уроков с курсами, это нужно что бы в настройках дней курса выбирать только из уроков доступных этому курсу.
CREATE TABLE IF NOT EXISTS course_lesson
(
    course_id  int UNSIGNED not null,
    lesson_id  int UNSIGNED not null,
    PRIMARY key course_lesson_assn_pk (course_id, lesson_id),
    constraint lesson_course_lesson_fk foreign key (lesson_id) references lessons(id),
    constraint lesson_course_course_fk foreign key (course_id) references courses(id),

    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()

);


-- Курс разбит на статии (этапы), например каждую неделю новая стадия
CREATE TABLE IF NOT EXISTS course_stages
(
    id           int UNSIGNED not null AUTO_INCREMENT primary key,
    name         varchar(255) not null,
    course_id    int unsigned not null,
    stage_number int unsigned not null,
    constraint course_stage_course_fk foreign key (course_id) references courses(id),
    created_at   datetime default NOW(),
    updated_at   datetime default NOW() on update NOW()
);


-- Таблица дней курса, каждый день входит в одну статию, день может быть обязательным для выполнения
create table if not EXISTS stage_days
(
    id              int UNSIGNED not null AUTO_INCREMENT primary key,
    name            varchar(255),
    is_required     bool     default false,
    course_stage_id int UNSIGNED not null,
    day_number      int unsigned not null,
    CONSTRAINT stage_days_course_stage_fk FOREIGN KEY (course_stage_id) REFERENCES course_stages (id),


    created_at      datetime default NOW(),
    updated_at      datetime default NOW() on update NOW()
);


-- таблица наборов уроков(сетов) внутри дней, сет может иметь разное кол-во повторов для круговых тренировок
-- курс без круговых тенировок имеет только один сет с одним повтором
create table if not EXISTS lesson_sets
(
    id                     int UNSIGNED not null AUTO_INCREMENT PRIMARY key, -- ??? нужен ИД для этой таблицы
    name                   varchar(255) not null,
    stage_day_id           int UNSIGNED not null,
    constraint lesson_sets_stage_day_fk foreign key (stage_day_id) references stage_days (id),
    repeat_count           int UNSIGNED not null default 1,
    user_repeat_count_from int UNSIGNED not null default 1,                  -- если это повторяемый сет, то пользователь может регулировать кол-во повторов в процессе тренировки
    user_repeat_count_to   int UNSIGNED not null default 1,                  -- в пределах этого диапазона
    created_at             datetime              default NOW(),
    updated_at             datetime              default NOW() on update NOW()
);


-- таблица для связи уроков и сетов уроков
create table if not exists lesson_set_lessons
(
    id            int unsigned not null auto_increment primary key, -- ключ нужен для внешних связе й
    lesson_id     int unsigned not null,
    lesson_set_id int unsigned not null,
    constraint lesson_set_lessons_lesson_fk foreign key (lesson_id) references lessons (id),
    constraint lesson_set_lessons_lesson_set_fk foreign key (lesson_set_id) references lesson_sets (id),
    # primary key lesson_set_lessons_pk (lesson_id, lesson_set_id),
    created_at    datetime default NOW(),
    updated_at    datetime default NOW() on update NOW()
);

-- поток курса, пользователи покупая курсы попадают в разные потоки, поток имеет определенную дату начала и окончания доступа к курсу
create table if not exists course_streams
(
    id         int unsigned not null auto_increment primary key,
    name       varchar(255),
    start_date date         not null,
    end_date   date         not null,
    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()
);


-- тиаблица связей потоков курсов и пользователей, запись создается при покупке
create table if not exists user_course_streams
(
    id               int unsigned not null auto_increment primary key, -- создаю отдельный первичный ключ для удобстав работы в прилоении, хотя тут состовной превичный ключ - пользователь + поток
    user_id          int unsigned not null,
    course_stream_id int unsigned not null,
    start_date       date         not null,                            -- дату старта и здута завершения переношу из данных потока при создании записи, это нужно,
    end_date         date         not null,                            -- так как после покупки пользователь не должен зависеть от настроек потока, у него могут быть индивидуальные даты или админ может из менять вручную

    constraint user_course_streams_user_fk foreign key (user_id) references users (id),
    constraint user_course_streams_stream_fk foreign key (course_stream_id) references course_streams (id),
    created_at       datetime default NOW(),
    updated_at       datetime default NOW() on update NOW()
);

-- домашние задания являются частью курсов и прикрепляются к дням курса
create table if not exists homeworks
(
    id         int unsigned not null auto_increment primary key,
    name       varchar(255) not null,

    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()
);

-- связь домашек и дней
create table if not exists stage_day_homework
(
    homework_id  int unsigned not null,
    stage_day_id int unsigned not null,
    primary key stage_day_homework_pk (homework_id, stage_day_id),
    constraint stage_day_homework_homework_fk foreign key (homework_id) references homeworks (id) on delete cascade on update cascade,
    constraint stage_day_homework_stage_day_fk foreign key (stage_day_id) references stage_days (id) on delete cascade on update cascade,

    created_at   datetime default NOW(),
    updated_at   datetime default NOW() on update NOW()
);


-- Таблица статуса дней у пользователя. Нужна для созранения даты начала и завершений дня пользователем. Для каждого потока из купленных им.
-- Возможно имеет смысл заменить эту таблицу на колоночную БД в которую сохранять действия. Типа: начал день, закончил день.
-- Тогда можно будет более гибко доставать статистику
create table if not exists user_day_status
(
    stage_day_id          int unsigned not null,
    user_course_stream_id int unsigned not null,
    start_time            datetime default null,
    end_time              datetime default null,

    primary key user_day_status_pk (stage_day_id, user_course_stream_id),
    constraint user_day_status_stage_day_fk foreign key (stage_day_id) references stage_days (id) on delete cascade on update cascade,
    constraint user_day_status_user_course_stream_fk foreign key (user_course_stream_id) references user_course_streams (id) on delete cascade on update cascade,

    created_at            datetime default NOW(),
    updated_at            datetime default NOW() on update NOW()

);

-- таблица статус уроков, хранит данные о времени просмотра каждого урока, нужна для статистики и подсчета просмотреного времени в дне,
-- на основе этого времени открывается доступ к следуюющим дням
-- аналогично статусам дней, вероятно, может быть заменена на таблицу из колоночной БД сохраняющей действия пользователей
create table if not exists user_lesson_status
(
    lesson_set_lesson_id  int unsigned not null,
    user_course_stream_id int unsigned not null,
    primary key user_lesson_status_fk (lesson_set_lesson_id, user_course_stream_id),
    constraint user_lesson_status_lesson_set_fk foreign key (lesson_set_lesson_id) references lesson_set_lessons (id) on delete cascade on update cascade,
    constraint user_lesson_status_user_course_stream_fk foreign key (user_course_stream_id) references user_course_streams (id) on delete cascade on update cascade,

    viewed_seconds        int unsigned not null default 0,
    play_count            int unsigned not null default 0,

    created_at            datetime              default NOW(),
    updated_at            datetime              default NOW() on update NOW()
);


-- статусы выполнения домашних заданий
create table if not exists user_homework_status
(
    stage_day_homework_id int unsigned not null,
    user_course_stream_id int unsigned not null,
    status                enum ('not_uploaded','uploaded','fail','done') default 'not_uploaded',
    rate                  int unsigned,

    created_at            datetime                                       default NOW(),
    updated_at            datetime                                       default NOW() on update NOW()
);

-- таблица тарифов для продажи курсов, разные тарифы могут иметь разные условия, и разные цены
create table if not exists tariffs
(
    id                int unsigned not null auto_increment primary key,
    name              varchar(255) not null,
    course_id         int unsigned,
    type              enum ('course_buy', 'course_extend','other'),
    sale_limit        int unsigned,
    payment_system_id int unsigned,
    constraint tariffs_course_fk foreign key (course_id) references courses (id),
    created_at        datetime default NOW(),
    updated_at        datetime default NOW() on update NOW()
);


-- у тарифов есть разные параметры, причем для разных типов тарифов параметры свои. Как ие параметры доступны какому типу определяется на уровне приложения. Бд тут только хранит данные
create table if not exists tariff_params
(
    id         int unsigned not null auto_increment primary key,
    tariff_id  int unsigned,
    type       enum ('course_stream_id','max_message_count','first_chat_message','extend_subscription_in_days'),
    value      varchar(255),
    constraint tariff_params_tariff_fk foreign key (tariff_id) references tariffs (id),

    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()

);
-- таблциа цен, цены различаются для разныю регионов. Цена вчера и сегодня указывается для акции типа - купи прямо сейчас, завтра будет дороже
create table if not exists prices
(
    id             int unsigned not null auto_increment primary key,
    tariff_id      int unsigned not null,
    zone           enum ('default','world','billions','capitals','ussr','usa','india','other'), -- ценовые зоны
    price_today    float        not null,
    price_tomorrow float,
    constraint prices_tariff_fk foreign key (tariff_id) references tariffs (id),

    created_at     datetime default NOW(),
    updated_at     datetime default NOW() on update NOW()

);

-- таблица заказов
create table if not exists orders
(
    id         int unsigned not null auto_increment primary key,
    user_id    int unsigned not null,
    note       varchar(255),

    constraint orders_user_fk foreign key (user_id) references users(id),

    created_at datetime default NOW(),
    updated_at datetime default NOW() on update NOW()
);

-- тарифы внутри заказов, цена тут должна фиксироваться в момент заказа
create table if not exists order_tariffs
(
    id         int unsigned not null auto_increment primary key,
    order_id   int unsigned not null,
    tariff_id  int unsigned not null,
    parent_id  int unsigned, -- для случая продажи бандга, когда в одном тарифе несколько других
    amount     double       not null,
    params     json         not null, -- параметры заказа на момент покупки [course_id, course_stream_id, stream_start_dat, stream_end_date]

    status     enum ('created','in_progress','paid','failed','canceled') default 'created', -- статусы оплаты
    constraint order_tariffs_tariff_fk foreign key (tariff_id) references tariffs(id),
    constraint order_tariffs_order_fk foreign key (order_id) references orders(id),
    constraint order_tariffs_parent_fk foreign key (parent_id) references order_tariffs(id),


    created_at datetime                                                  default NOW(),
    updated_at datetime                                                  default NOW() on update NOW()
);


-- счет создается в момент оплаты, общая стоиомтьс фикстуриется на момент оплаты
create table if not exists invoices
(
    id              int unsigned       not null auto_increment primary key,
    order_id        int unsigned       not null,
    amount          double             not null,
    payment_systems enum ('ps1','ps2') not null                               default 'ps1',
    status          enum ('created','in_progress','paid','failed','canceled') default 'created', -- аналогичные заказу статусы оплаты

    constraint invoices_order_fk foreign key (order_id) references orders(id),

    created_at      datetime                                                  default NOW(),
    updated_at      datetime                                                  default NOW() on update NOW()
)

