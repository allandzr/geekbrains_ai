import os
import random
import string


def random_string():
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(50))


def create_files(path):
    for i in range(1, 3):
        with open(path + '/' + str(i) + '.txt', 'w') as f:
            f.write(random_string())


def create_month_dir(path):
    for i in range(1, 13):
        dir_name = path + '/' + str(i)
        os.mkdir(dir_name)
        create_files(dir_name)


for i in range(2010, 2018):
    dir_name = str(i)
    os.mkdir(dir_name)
    create_month_dir(dir_name)
