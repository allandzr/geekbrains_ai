from methods import explode_num
from methods import reverse_array


num = int(input('Enter number: '))
s = ''
for i in reverse_array(explode_num(num)):
    s += str(i)

print(int(s))