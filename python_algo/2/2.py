from methods import count_even_odd


num = int(input('Enter number: '))
even, odd = count_even_odd(num)
print(f"Number {num} even: {even} odd: {odd}")


