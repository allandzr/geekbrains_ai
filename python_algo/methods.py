def explode_num(num):
    e = []
    while num >= 10:
        e.append(num % 10)
        num = num // 10

    e.append(num)

    return reverse_array(e)


def reverse_array(array):
    r = []
    i = len(array) - 1
    while i >= 0:
        r.append(array[i])
        i -= 1
    return r


def count_even_odd(num):
    even, odd = 0, 0
    for n in explode_num(num):
        if n % 2 == 0:
            even += 1
        else:
            odd += 1
    return even, odd
