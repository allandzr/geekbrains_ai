# уровнение прямой (y1 -y2)x + (x2-x1)y + (x1y2 - x2y1)
# чрезе точки с координатами (x1, y1) и (x2, y2)

x1, y1 = int(input('X1: ')), int(input('Y1: '))
x2, y2 = int(input('X2: ')), int(input('Y2: '))

a = y1 - y2
b = x2 - x1
c = x1 * y2 - x2 * y1
print(f'{a}x + {b}y = {-c}')
