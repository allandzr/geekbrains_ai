#4. Пользователь вводит две буквы. Определить, на каких местах алфавита они стоят, и сколько между ними находится букв.


s = 'abcdefghijklmnopqrstuvwxyz'

a,b = input('Input char a').lower(), input('Input char b').lower()


pos_a = s.find(a) + 1
pos_b = s.find(b) + 1


print(f'pos a: {pos_a}')
print(f'pos b: {pos_b}')
print(f'distance {abs(pos_a-pos_b)}')